public class Triangle {
    private float a, b, c, sideLength = 0;

    public Triangle(float sideLength) {
        this.sideLength = sideLength;
    }
    public Triangle(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getC() {
        return c;
    }

    public void setC(float c) {
        this.c = c;
    }

    public float getSideLength() {
        return sideLength;
    }

    public void setSideLength(float sideLength) {
        this.sideLength = sideLength;
    }

    public float calculatePerimeter() {
        float perimeter;
        if (sideLength != 0) {
            perimeter = sideLength * 3;
        } else {
            perimeter = a + b + c;
        }
        return  perimeter;
    }
}
